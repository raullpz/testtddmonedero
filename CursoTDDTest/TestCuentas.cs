using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CursoTDDTest
{
    [TestClass]
    public class TestCuentas
    {

        [TestMethod]
        public void Crear_Cuenta_Saldo_Cero()
        {

            //Arrange
            Cuentas cuenta = new Cuentas();

            //Act

            //Assert
            Assert.AreEqual(0, cuenta.saldo);

        }


        [TestMethod]
        public void Al_Ingresar_100_Cuenta_Vacia_Saldo_100()
        {

            //Arrange
            Cuentas cuenta = new Cuentas();

            //Act
            cuenta.IngresarSaldo(100);

            //Assert
            Assert.AreEqual(100, cuenta.saldo);

        }

        [TestMethod]
        public void Al_Ingresar_3000_Cuenta_Vacia_Saldo_3000()
        {

            //Arrange
            Cuentas cuenta = new Cuentas();

            //Act
            cuenta.IngresarSaldo(3000);

            //Assert
            Assert.AreEqual(3000, cuenta.saldo);

        }

        [TestMethod]
        public void Al_Ingresar_3000_Cuenta_100_Saldo_3100()
        {

            //Arrange
            Cuentas cuenta = new Cuentas();
            cuenta.IngresarSaldo(100);

            //Act
            cuenta.IngresarSaldo(3000);

            //Assert
            Assert.AreEqual(3100, cuenta.saldo);

        }


        [TestMethod]
        public void Al_Ingresar_SaldoNegativo_Cuenta_Vacia_Saldo_0()
        {

            //Arrange
            Cuentas cuenta = new Cuentas();

            //Act
            cuenta.IngresarSaldo(-100);

            //Assert
            Assert.AreEqual(0, cuenta.saldo);

        }


        [TestMethod]
        public void Al_Ingresar_Saldo3Decimales_Cuenta_Vacia_Saldo_0()
        {

            //Arrange
            Cuentas cuenta = new Cuentas();

            //Act
            cuenta.IngresarSaldo(100.457);

            //Assert
            Assert.AreEqual(0, cuenta.saldo);

        }


        [TestMethod]
        public void Al_Ingresar_Saldo6000_Cuenta_Vacia_Saldo_6000()
        {

            //Arrange
            Cuentas cuenta = new Cuentas();

            //Act
            cuenta.IngresarSaldo(6000.00);

            //Assert
            Assert.AreEqual(6000.00, cuenta.saldo);

        }

        [TestMethod]
        public void Al_Ingresar_Saldo_Mayor_6000_Cuenta_Vacia_Saldo_0()
        {

            //Arrange
            Cuentas cuenta = new Cuentas();

            //Act
            cuenta.IngresarSaldo(6000.01);

            //Assert
            Assert.AreEqual(0, cuenta.saldo);

        }

    }


}
