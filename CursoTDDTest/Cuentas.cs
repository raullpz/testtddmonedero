﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CursoTDDTest
{

    public class Cuentas
    {
        public double saldo { get; set; }

        public Cuentas()
        {
            saldo = 0;
        }

        public void IngresarSaldo(double ingreso)
        {

            if ((ingreso > 0) && (ingreso <= 6000))
            {

                if (Math.Round(ingreso, 2) == ingreso)
                    saldo += ingreso;

            }

        }

        public void feature001()
        {

        }

        public void new001_1()
        {

        }
               
        public void new002()
        {

        }

        public void new003() {
        }
        

        public void new004()
        {

        }


    }

}
